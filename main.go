package main

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/jwtauth/v5"
	httpSwagger "github.com/swaggo/http-swagger"
	"gitlab/jwt/addr"
	_ "gitlab/jwt/docs"
	"gitlab/jwt/jwt"
	"log"
	"net/http"
)

// @title задача 1.6.3 & задача 1.7.4
// @version 2.0
// @description  Геосервис API с авторизацией
// @host localhost:8080
// @BasePath /

//	@securityDefinitions.apikey	Bearer
//	@in							header
//	@name						Authorization
//	@description The following syntax must be used in the 'Authorization' header: Bearer: xxxxxx.yyyyyyy.zzzzzz

func main() {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	// Protected routes
	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(jwt.TokenAuth))
		r.Use(jwtauth.Authenticator(jwt.TokenAuth))

		r.Post("/api/address/search", addr.Search)   //search.go
		r.Post("/api/address/geocode", addr.Geocode) //geocode.go
	})

	// Public routes
	r.Group(func(r chi.Router) {

		r.Get("/swagger/*", httpSwagger.Handler(
			httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
		))

		r.Post("/api/login", jwt.LoginHandler)
		r.Post("/api/register", jwt.RegistrationHandler)

	})
	log.Println("Server being started. Swagger: http://localhost:8080/swagger/")
	log.Fatalln(http.ListenAndServe(":8080", r))
}
