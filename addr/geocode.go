package addr

import (
	"encoding/json"
	"net/http"
)

type GeocodeRequest struct {
	Lat string `json:"lat" example:"55.753994" validate:"required"`
	Lng string `json:"lon" example:"37.620003" validate:"required"`
}

type GeocodeResponse struct {
	Addresses []*Address `json:"suggestions"`
}

// Geocode
// @summary 		Получить адрес по координатам
// @Description 	Получить адрес по координатам
// @Tags 			geocode
// @Accept 			json
// @Produce 		json
// @Security 		Bearer
//
// param 	param name, param type, data type, is mandatory?, comment attribute(optional)
//
// @Param			request body 	addr.GeocodeRequest true "Geocode request"
// @Success 		200 {object} 	addr.GeocodeResponse
// @Failure 		400	{string}	string "Bad request"
// @Failure 		401	{string}	string "Unauthorized"
// @Failure 		500	{string}	string "Internal server error"
// @Router 			/api/address/geocode [post]
func Geocode(w http.ResponseWriter, r *http.Request) {
	// check for POST method
	if r.Method != http.MethodPost {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	var gr GeocodeRequest
	err := json.NewDecoder(r.Body).Decode(&gr)
	if err != nil {
		http.Error(w, "Error decoding request body: "+err.Error(), http.StatusBadRequest)
		return
	}

	// doRequest() from apiclient.go
	resp, err := doRequest(&gr)
	if err != nil {
		http.Error(w, "Error executing request: "+err.Error(), http.StatusInternalServerError)
		return
	}

	var geocodeResponse *GeocodeResponse
	err = json.NewDecoder(resp.Body).Decode(&geocodeResponse)
	if err != nil {
		http.Error(w, "Error decoding response body: "+err.Error(), http.StatusInternalServerError)
		return
	}

	jsonBytes, err := json.Marshal(geocodeResponse)
	if err != nil {
		http.Error(w, "Error marshalling response: "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonBytes)
}

func init() {
	http.HandleFunc("/api/address/geocode", Geocode)
}
