package jwt

import (
	"encoding/json"
	"golang.org/x/crypto/bcrypt"
	"net/http"
)

// LoginHandler - login user
// @summary 		Логин пользователя
// @description 	Логин пользователя.
// @Tags 			auth
// @Accept 			json
// @Produce 		json
// @Param 			request body jwt.UserReq true "User request"
// @success 200 {string} string "User logged in"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Invalid credentials"
// @Router /api/login [post]
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	userReq := UserReq{}

	err := json.NewDecoder(r.Body).Decode(&userReq)
	if err != nil {
		http.Error(w, "Error decoding request body: "+err.Error(), http.StatusBadRequest)
		return
	}

	// проверка пароля и что пользователь существует
	if _, ok := UsersDB[userReq.Username]; !ok || bcrypt.CompareHashAndPassword([]byte(UsersDB[userReq.Username]), []byte(userReq.Password)) != nil {
		http.Error(w, "Invalid credentials", http.StatusUnauthorized)
		return
	}

	_, tokenString, _ := TokenAuth.Encode(map[string]interface{}{"username": userReq.Username, "hash": UsersDB[userReq.Username]})

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(tokenString))
}
