FROM golang:1.19-alpine AS builder

WORKDIR /app

COPY ["./", "./"]

RUN go mod download

RUN go build -o ./bin/cmd gitlab/jwt

FROM alpine

COPY --from=builder /app/bin/cmd ./

EXPOSE 8080

CMD ["/cmd"]